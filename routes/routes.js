const express = require('express');
const user_controller = require('../controller/users');

const router = express.Router();

router.get('/', user_controller.home);
router.post('/add', user_controller.addUser);
router.get('/:id', user_controller.user_details);
router.delete('/:id', user_controller.delete_user);
router.put('/update/:id', user_controller.user_update);

module.exports = router;
