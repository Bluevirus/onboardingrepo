const logger = require('../logger/logger');
//middleware that handles all api and log into log file
exports.apihandler = (req, res, next) =>{
    logger.info('new request with end point : '+req.url)
        next();
}