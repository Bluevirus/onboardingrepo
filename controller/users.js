const User = require('../model/users');
const redis = require('redis');
const client = redis.createClient();
const logger = require('../logger/logger');

client.on('error', (err) => {
    logger.error('Redis Error'+err);
    console.log("Error " + err);
});

//Simple version, without validation or sanitation
exports.home = function (req, res) {
    console.log(req.params);
    res.send('This is User Home page');
};

exports.addUser = function (req, res) {
    let user = new User(
        {
            userid : req.body.userid,
            email: req.body.email,
            name: req.body.name
        }
    );
   
    user.save(function (err) {
        if (err) {
            res.send(req.body);
        }
        client.publish('notification','user saved to mongodb ', function(){
            logger.info('Published data on notification channel');
            console.log('publisher has sucessfully published saved data ');
        });
        res.send('User Added successfully')
    })
};

exports.user_details = function(req, res){
    console.log(req.params);
   
    client.get(req.params.id, function (err, result) {
        if(result){
            console.log('data fetched from redis');
            res.send(JSON.parse(result));
        }
        else{
            User.findById(req.params.id, function (err, result) {
                if (err) return next(err);
                console.log('data fetched from mongodb');
                client.setex(req.params.id, 360, JSON.stringify(result));
                res.send(result);
            })
        }   
    })
}

exports.delete_user = function(req, res){
    console.log(req.params);
    User.findByIdAndDelete(req.params.id, function(err, user){
        if (err) return next(err);
        console.log('user deleted');
        client.publish('notification','user deleted from database ', function(){
            logger.info('Published data on notification channel');
            console.log('publisher has sucessfully published deleted info');
        });
        res.send(user);
    });
}

exports.user_update = function(req, res){
    User.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, user) {
            if (err) return next(err);
            console.log('user updated');
            client.publish('notification','user updated to database ', function(){
                logger.info('Published data on notification channel');
                console.log('publisher has sucessfully published updated info');
            });
            res.send(user);
        })
}