const redis = require('redis');
const logger = require('../logger/logger');

const  subscriber = redis.createClient();

subscriber.on('message', function (channel, message) {
    logger.info('subscriber have accepted message : '+message+' and the channel is : '+channel)
    console.log('message is : '+ message+ 'and the channel name is :'+ channel);
});
subscriber.subscribe('notification');