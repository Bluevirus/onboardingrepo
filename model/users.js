const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    
    userid : {
        type : Number,
        required : true,
    }, 
    email: {
        type: String,
        required: true,
        max: 100
    },

    name: {
        type: String, 
        required: 100
    }
});

// Export the model
module.exports = mongoose.model('User', UserSchema);