const express = require('express');
const bodyParser = require('body-parser');
const UsersRoute = require('./routes/routes');
const mongoose = require('mongoose');
const responseTime = require('response-time');
const socketio = require('socket.io');
const dotenv = require('dotenv');
const middleware = require('./middleware/middleware');
const logger = require('./logger/logger');
const { Logger } = require('log4js');

const app = express();

dotenv.config();
const port = process.env.PORT;
const mongoDB = process.env.MONGODB_URI;

mongoose.connect(mongoDB);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection failed:'));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(middleware.apihandler);
app.use('/user', UsersRoute);

app.listen(port, function() {
    logger.info('server started on port '+port)
    console.log('listening on '+ port)
})